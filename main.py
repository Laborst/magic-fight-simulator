from Magician import Magician
from Healer import Healer


mag = Magician("Steven")
healer = Healer("Avicenna")

while mag.health > 0 and healer.health > 0:
    healer.sufferFromDamage(mag.swipe())
    mag.sufferFromDamage(healer.weakPunch())
    healer.heal()

if healer.health > mag.health:
    print('{} winner!'.format(healer.name))
else:
    print('{} winner!'.format(mag.name))
