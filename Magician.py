class Magician:

    def __init__(self, name):
        self.name = name
        self.mana = 50
        self.health = 50
        self.healthStatus = 'alive'
        self.manaStatus = 'powerful'

    def weakPunch(self):
        self.mana -= 5
        print('{} lost 5 mana points'.format(self.name))
        return 5

    def swipe(self):
        self.mana -= 15
        print('{} lost 15 mana points'.format(self.name))
        return 15

    def sufferFromDamage(self, damage):
        self.health -= damage
        print('{} lost {} health points'.format(self.name, damage))

